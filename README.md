# Database as a Service - Evaluacion de proveedores de nube

## Necesidad
![Arquitectura Actual Velose](//pictures/velose_arquitectura_actual_ii.png)

La solución **Velose** ha nacido con las siguientes necesidades tecnológicas:
1. PSE-OSE
    * Tiempo de procesamiento de comprobantes: 5 segundos.
    * SLA: 99.96% anual.
2. OSE-SUNAT
    * Tiempo de procesamiento de comprobantes: 1 hora.

La implementación actual del sistema **Velose** carece de las siguientes características, a nivel de Base de Datos:
***

#### Alta disponibilidad (HA):
Actualmente no se maneja un mecanismo de fail-over/read replicas en ninguna de las bases de datos (solo existe un mecanismo master-slave), no permitiendo a TCI cumplir el SLA impuesto por SUNAT.

![Esquema HA MySQL](//pictures/mysql-high-availability-drbd-replication.jpg)

En la ilustración mostrada, si la base de datos principal **(Main)** sufre algun tipo de problema (ejem: corte electrico en la region/zona donde se ubica), en unos segundos la replica **(Backup)** pasa a ser la base de datos principal. Esto se logra ya que ambas bases de datos mantienen una sincronización de datos que puede ser sincrona ( -tiempo de sincronizacion pero + carga) o asíncrona. Tambien se debe de considerar que la base de datos principal y replica(s) deben de estar ubicados en distintas zonas dentro de una region ó en distintas regiones, con el fin de ofrecer un HA más robusto.
***

#### Seguridad:
Nuestra implementación on-premise de base de datos no cumple con los siguientes estándares de seguridad: [ISO 27001](https://www.iso.org/isoiec-27001-information-security.html), [PCI DDS](https://www.pcisecuritystandards.org/pci_security/), [SSAE](http://ssae16.com/SSAE16_overview.html), etc. 

> Como ejemplo: Las conexiones hacia las bases de datos se hacen sin implementar TLS/SSL, haciendo que los registros puedan ser capturados por la red sin problema alguno.
***

#### Escalabilidad Vertical
Actualmente para poder incrementar la capacidad de nuestra base de datos requerimos hacer los siguientes pasos: 
1. Realizar el cambio desde la consola administrativa de IBM.
2. Reiniciar el servidor de IBM.

Este procedimiento hace que, ante un pico de procesamiento de comprobantes, tengamos que reiniciar nuestros servidores para poder atender la demanda. Sumado al hecho que no poseemos un mecanismo de alta disponibilidad, este procedimiento impacta nuestro SLA.

En un esquema de escalabilidad vertical automatico, __es TCI quien decide aplicar estos cambios y son aplicados en menor tiempo, ya que es en linea__.
***

#### Backups automaticos
Nuestra implementacion no posee un mecanismo automático de backups, por lo que ante una perdida de disco por falla de disco no tenemos como recuperarnos. Asi mismo, si queremos replicar la informacion que tenemos en producto para pruebas de estres tenemos que interactuar con la base de datos en producción para la copia de información, comprometiendo nuestro SLA.
***

#### Personal especializado en base de datos
En la solucion Velose, son los desarrolladores son quienes deciden la estructura de tablas/indices y el uso de caracteristicas más avanzadas según la base de datos. Sin embargo, en un esquema de alta disponilidad/replicacion de información se necesita un conocimiento más profundo de c/u de las soluciones de BD usadas, expertise que ninguno de nuestros desarrolladores posee ni es entrenado para obtenerlo.
***

#### Observabilidad
Para cubrir este punto, actualmente estamos usando una solucion free pero limitada que nos permite ver metricas de nuestras BDs. Sin embargo, para observar los logs tenemos que entrar a los servidores y descargar los archivos correspondientes, siendo un proceso lento.
<br/>

![Esquema HA MySQL](//pictures/stats.png)
<br/>

![Esquema HA MySQL](//pictures/statsII.png)
<br/>

En ambos gráficos podemos ver las herramientas que ofrece Google Cloud Platform para observabilidad de Base de Datos MySQL: En el primero, vemos una métrica de operaciones IO en una hora; mientras que en la segunda, podemos ver y filtrar los logs generados por la base de datos.
***

En base a estas necesidades se ha decidido evaluar a los 4 proveedores de nube más importantes actualmente (Amazon, Azure, Google e IBM) y ver que solución ofrecen c/u de ellos a este problema.<br/><br/>

## Criterio de Evaluación:
1. Costo de la maquina virtual que va a correr la base de datos, principalmente:
    1. Costo de CPU/RAM/HDD (o SSD).
    2. Número de maquinas virtuales a correr, para lograr una robusta HA.
    3. Localización de las máquinas (region/zona). 

2. Características adicionales, como:
    1. Costo de guardado de backups.
    2. Monitoreo del comportamiento de la base de datos.
    3. Funcionalidad que ofrece para lograr HA.
    4. Seguridad (algunas de estas funcionalidades dependen propiamente del motor de base de datos):
        1. Definición de usuarios y roles.
        2. Métodos de conexión hacia la base de datos.
        3. IP Whitelisting/Black List.
---

## IBM Compose MySQL

####  Características:
- Ofrece soluciones de base de datos tanto para [MongoDB](https://www.compose.com/databases/mongodb) como para [MySQL](https://www.compose.com/databases/mysql) (esta aún se encuentra en estado beta).<br/><br/>

- La documentacion de Compose se encuentra en el siguiente link [siguiente link](https://help.compose.com/).<br/><br/>

- Compose usa el concepto de **deployment**, que es la unidad mínima de base de datos que se ofrece como servicio. Por ejemplo: Un deployment de MySQL en Compose consiste en un cluster MySQL con 3 nodos (1 master + 2 replicas) + dos portales HAProxy para acceso exterior a MySQL.<br/><br/>

- Asi mismo, tambien maneja el concepto de **unidad**, que es la representación de 1 GB de disco duro. Al momento de crear un deployment, Compose ofrece los recursos de hardware en un ratio de conversión de 10(unidades):1(GB RAM). Esto quiere decir que si se desea incrementar 1 GB de RAM, obtendremos sí o sí un incremento de 10GB de disco duro por defecto.<br/><br/>

- Finalmente, el concepto de **auto-escalamiento** ofrece la capacidad de escalar verticalmente un deployment de manera automática. Para hacerlo, Compose monitorea c/hora el deployment y si detecta que los recursos se encuentran a tope decide hacer un incremento siguiendo el ratio de 10:1 antes mencionado. Se debe de tener en cuenta que este es un va(valor por defecto, se puede incrementar el ratio desde la consola web).
<br/>
    > Compose no puede realizar la operación inversa de manera automática. Esto es, si necesita reducir la capacidad de disco duro/RAM se debe de hacer desde su interfaz web.

<br/>

##### MySQL: (https://help.compose.com/docs/mysql-compose-for-mysql)
- Un deployment está conformado por 1 nodo maestro y 2 nodos replicas de lectura, ubicados en distintas zonas dentro de una misma región (Las zonas y regiones usadas estan detalladas en https://help.compose.com/docs/compose-datacenter-availability). Si el lider falla, automaticamente se ejecuta la promocion de una replica como lider dentro de 60 segundos despues de la falla del lider.<br/><br/>
- Tambien el deployment contiene 2 servidores HAProxy, cuya función es monitorear constantemente quien es el lider del cluster MySQL, con el fin de que el las conexiones entrantes no se preocupen por determinar quien es el lider del cluster.<br/><br/>

- **Backups**: Los backups pueden ser tomados diaria, semanal, mensualmente o a demanda (solo en la interfaz web).
La documentacion no describe los costos de backup, pero si la estrategia de retención de backups:
    - Para backups realizados diariamente, la retencion es de 7 dias.
    - Para backups semanales, la retencion es de 4 semanas.
    - Para backups mensuales, la retencion es de 3 meses.
<br/>

- **Seguridad**: Compose ofrece un dominio publico desde el cual nuestras aplicaciones pueden conectarse usando SSL por defecto. Asi mismo, los usuarios y permisos para la conexión son creados desde MySQL.
La documentación no brinda información sobre si se puede restringir la conexión desde una serie de IPs (ip whitelisting).<br/><br/>
    
- **Costo** : Segun el cálculo hecho en https://www.compose.com/pricing#mysql, el costo de un deployment con 80GB de storage y 8GB de RAM cuesta **1,449 dolares mensuales**. Se paga $18 dolares mensuales por cada 1 unidad extra a agregar al deployment.<br/><br/>

##### MongoDB: (https://help.compose.com/docs/mongodb-on-compose)
- Características y Costos similares a los mostrados en MySQL, la única diferencia se encuentra en el motor de persistencia (WiredTriger vs MMAPv1).
<br/>
---
## Microsoft Azure
Azure nos ofrece productos distintos para c/u de las bases de datos usadas en TCI. Para el caso de MySQL ofrece **Azure Database for MySQL** (https://azure.microsoft.com/en-us/services/mysql/) y para MongoDB existen dos alternativas: **Azure CosmosDB** (https://azure.microsoft.com/en-us/services/cosmos-db/) o **MongoDB Atlas** (https://www.mongodb.com/cloud/atlas), una solución creada por la empresa fundadora de MongoDB y que puede correr en Google Cloud Platform, Azure o AWS.
<br/>

### Azure Database for MySQL
#### Caracteristicas
- Los costos estan asociados principalmente según la región en donde se cree la base de datos. El detalle de las regiones se encuentra en el siguiente enlace: https://docs.microsoft.com/en-gb/azure/mysql/concepts-pricing-tiers
<br/>

- El detalle de los costos a pagar por cualquiera de estas soluciones incluye lo siguiente:
    - Localización (Región/Zona).
    - Tipo de Instancia MySQL: 
        - Dividido por capas: Basic, General Purpose y Memory Optimised
        - Generación:
            - La generacion 4 son máquinas basadas en procesadores Intel E5-2673 v3 (Haswell) de 2.4 GHz.
            - La generacion 5 son máquinas basadas en procesadores Intel E5-2673 v4 (Broadwell) 2.3 GHz.
        - Cantidad de vCores.
    - Alta disponibilidad disponible desde la creación de la base de datos.
    - Tamaño del storage para datos (SSD).
    - Tamaño del storage y localización de Backups.
    - Numero de Replicas (read replicas).
    - Proteccion ante amenazas.
    - Ancho de banda usado para transferencia de datos.
<br/>

- Ajuste performance y escalabilidad en segundos: Azure ofrece tres capas según la carga de trabajo que tengan sus clientes: Capa Básica (**Basic**), Capa de Propósito General (**General Purpose**) y Capa de Memoria Optimizada (**Memory Optimised**). La cantidad de CPU, RAM y Disco Duro disponible para una base de datos depende de la capa con la que se desee trabajar:
<br/>

    ![Caracteristicas Por Capa I](//pictures/azure_mysql_features_per_tier.png)
    <br/>

- El número máximo de conexiones por cada vCore es la siguiente:
<br/>

    ![Máximo de conexiones](//pictures/azure/azure_mysql_max_connections.png)
    <br/>

- **Storage**
    <br/>

    - El costo de storage esta asociado a la región en donde se cree la base de datos. Por ejemplo, en North Central US cuesta **0.115 dolares mensuales por GB** . La documentacion no especifica si se usa SSD o HDD como storage, pero en base a un articulo publicado [aquí](https://medium.com/@lakshmanLD/comparison-of-mysql-across-aws-azure-and-gcp-19af2d208d9a) se entiende que Azure solamente maneja discos SSD. Las caracteristicas de cada tipo de storage estan detalladas en el siguiente cuadro:
    <br/>

        ![Caracteristicas De Storage](//pictures/azure_mysql_features_per_tierIII.png)
        <br/>

    - Azure marca el servidor MySQL a modo de **solo lectura** cuando quede un 5% disponible de storage. Esto quiere decir que si se hace una reserva de 100GB y storage y si el actual uso del disco duro llega a 95GB, el servidor entra a modo de solo lectura. El incremento de disco se puede hacer de modo automatico desde la consola web o de comandos de Azure, sin embargo se recomienda crear alarmas en base al uso del storage para evitar estos contratiempos.
    <br/>

- **Alta Disponibilidad (HA)**
    <br/>

    - Azure ofrece un SLA de 99.99% sobre alta disponibilidad. Para lograr esto, su modelo de HA esta basado en un mecanismo propio de failover cuando un servidor de base de datos deja de funcionar. A diferencia de otras soluciones que mantienen un servidor failover en modo stand-by, cuando ocurre este fallo Azure levanta una nueva instancia de la base de datos y le adjunta el storage que hayamos estado usando. Azure (y la mayoria de proveedores de nubes) nos indican que la alta disponibilidad se maneja desde dos frentes: **desde la misma base de datos** (según el mecanismo detallado arriba) y **desde el pool de conexiones de c/aplicación** que interactue con la base de datos, siendo la última de completa responsabilidad del equipo de desarrollo de TCI.
    <br/>

- **Replicacion**
    <br/>
    **TODO:**

- **Backups**
    <br/>
    - Azure crea backups automaticamente de la base de datos, con un periodo de retención desde 7 días hasta 35 (puede ser ajustada en cualquier momento desde la interfaz de Azure). Además, Azure no cobra por backup storage menor al 100% del tamaño del storage que estemos usando. Por ejemplo, si nuestro storage es de 100GB tendremos de forma gratuita backup storage de 100GB. Una ves pasada esta marca, los costos varian según el tipo de redundancia que se busque tener:
    <br/>

        ![Costos de Backup](//pictures/azure_backup_price.png)
        <br/>

    - Azure toma tres tipos de backups: total, diferencial y de logs de transacciones. Generalmente, los backups totales ocurren cada semana; los diferenciales ocurren 2 veces por dia y los logs de transacciones ocurren cada 5 minutos.
    <br/>

- **Seguridad**
    <br/>
    
    - El firewall que posee este servicio por defecto bloquea todas las conexiones entrantes hacia la base de datos. Para empezar a usar nuestro servidor desde otra computadora, debemos de especificar una o más reglas desde la consola web de Azure. En el caso de que esta computadora se encuentra dentro de Azure (como es el caso de maquinas virtuales o aplicaciones corriendo en Azure) se debe de habilitar **Azure Connections** (https://docs.microsoft.com/en-gb/azure/mysql/concepts-firewall-rules#connecting-from-azure).
    <br/>

    ![Firewall](//pictures/azure_firewall_rules.png)
    <br/>

    - Asi mismo, Azure ofrece un mecanismo más avanzado de seguridad llamada [Advanced Threat Protection](https://docs.microsoft.com/en-gb/azure/mysql/concepts-data-access-and-security-threat-protection) que permite, entre otras cosas, detectar accesos desde localizaciones desconocidas o desde aplicaciones desconocidas y accesos de tipo fuerza bruta.
    <br/>

- **Metricas**
    <br/>

    - Azure provee varias metricas que brindan insight sobre el comportamiento de los recursos asignados al servidor MySQL. Todas las métricas tienen una frecuencia de **1 minuto** y son retenidas por **30 dias**. Asu ves, tambien se pueden configurar alertas sobre estas métricas (mayor información en: https://docs.microsoft.com/en-gb/azure/mysql/howto-alert-on-metric). Finalmente, Azure brinda [otras funcionalidades](https://docs.microsoft.com/en-gb/azure/monitoring-and-diagnostics/monitoring-overview-metrics) en base a las métricas como configurar acciones automáticas, análisis de métricas y historial. Esta es la lista de métricas que ofrece Azure para MySQL:
    <br/>

    ![Metricas](//pictures/azure_mysql_metricas.png)

#### Costo
- Basados en la documentación de Azure Database for MySQL: https://azure.microsoft.com/en-us/pricing/details/mysql/, se estima el costo para la region de Central US.

    | Recurso              | Descripción                                         | Precio Total (x mes)  |
    | -------------------- |:---------------------------------------------------:| ---------------------:|
    | Instancia            | Gen 5, 4 vCore General Purpose                      | $306.90               |
    | Storage              | 500GB SSD ($0.138 x GB)                             | $69.00                |
    | Backup               | Primeros 500GB ($0.00 x GB)                         | $0.00                 |
    | Backup               | 500GB ($0.12 x GB)                                  | $60.00                |
    | Transf. de Datos     | Ingress                                             | $0.00                 |
    | Transf. de Datos     | Egress (IBM -> Azure): $0.087 x 100 GB              | $8.70                 |
    | **Total**            | --                                                  | **$444.60**           |
<br/>

- Si hicieramos una migración completa de nuestros servicios a Azure los siguientes precios cambiarian:

    | Recurso              | Descripción                                         | Precio Total (x mes)  |
    | -------------------- |:---------------------------------------------------:| ---------------------:|
    | Transf. de Datos     | Egress (Azure -> Azure ):  $0.01 x 100 GB           | $1.00                 |
    | **Total**            | --                                                  | **$436.60**           |
<br/>

---    
## Google Cloud Platform
GCP no posee un servicio orientado a MongoDB (pero sí a bases de datos [orientadas a documentos](https://cloud.google.com/datastore/docs/concepts/overview)), por lo que MongoDB Atlas sigue siendo la alternativa más factible. Para el caso de bases de datos relacionales, este proveedor ofrece el servicio [Cloud SQL](https://cloud.google.com/sql/) tanto para MySQL como para Postgre.
<br/>

### Google Cloud SQL for MySQL
#### Caracteristicas

- Similar a Azure, los costos estan asociados principalmente a la localización (Zona/Region) en donde se cree la base de datos. El detalle de las zonas/regiones se encuentra en el siguiente link: https://cloud.google.com/compute/docs/regions-zones/.
<br/>

- Los recursos a tener en cuenta para el calculo de costos de la base de datos comprende:
    - Localización (Región/Zona).
    - Tipo de instancia SQL:
        - Las instancias de primera generación soportan MySQL 5.5 y 5.6, pueden ser provistas de hasta 16GB de RAM y 500GB de storage. No se puede incrementar el tamaño de storage a demanda.
        - Las instancias de segunda generación soportan MySQL 5.6 y 5.7, pueden ser provistas de hasta de 416 GB de RAM y 10TB de storage, con la opción de incrementar automáticamente el storage según demanda.
    - Alta disponibilidad disponible desde la creación de la base de datos.
    - Tamaño del storage para datos y tipo (HDD y SSD).
    - Tamaño del storage para backups.
    - Transferencia de datos.
<br/>

        ![Tipos de instancias](//pictures/gcp/cloud_mysql_instances.png)
<br/>

- Descuentos de uso sostenido: GCP ofrece descuentos automaticos según cuanto tiempo ha sido usado una máquina virtual por un mes (se determina el descuento por cada minuto en el que se usa la instancia). Ya que Google Cloud SQL en el fondo es una máquina virtual administrada por GCP, el descuento que se puede llegar a obtener en un mes **es del 30% del precio**. Para más información, revisar [el siguiente enlace](https://cloud.google.com/compute/docs/sustained-use-discounts).
<br/>

- Las **actualizaciones de software** en Cloud SQL (maintenance window) pueden ser configuradas para ejecutarse en un intervalo de horas y dia de semana delimitado.
<br/>

- **Storage**
    <br/>

    - Una ves creada una instancia, es permitido el incremento de capacidad del storage pero no su disminución. Si se habilita el incremento automático del storage, este es revisado cada 30 segundos y si se detecta que el storage disponible se encuentra menor a cierta cantidad (**umbral**) se adiciona más storage para la instancia.
    <br/>

    - El calculo del umbral se aplica del siguiente modo:
        - Mayor o igual a 500GB = 25GB es lo mínimo permitido.
        - Menor a 500GB         = 5 + (provisioned storage)/25.
    <br/>
    - El monto de storage añadido es igual al umbral.
    <br/>

- **Alta disponibilidad (HA)**
    <br/>

    ![Configuración HA](//pictures/gcp/ha-mysql-config.svg)
    <br/>

    - Para proveer esta característica, la configuración de HA esta compuesta por una instancia primaria (**_master_**) en la zona primaria y una **_replica failover_** en la zona secundaria. Atraves de una **_replicación semi-sincrona_**, todos los cambios hechos en la data y tablas de usuario en la zona primaria son copiados en la replica failover. Esta configuración reduce el tiempo de inactividad en caso de fallos, permitiendo que la data este siempre disponible.
    <br/>

    - La replica failover es **cobrada como una instancia separada**. Posee los mismos flags, usuarios, passwords, aplicaciones y conexiones autorizadas que la instancia primaria. Las limitaciones de esta instancia son las siguientes:
        - La instancia primaria y la replica failover deben de estar en la misma región, distinta zona.
        - Solo se puede crear una replica failover por instancia primaria.
        - Tiene la misma ventana de mantenimiento que la instancia primaria, y no puede ser modificada.
        - No se pueden habilitar backups para esta instancia.
    <br/>

    - Comportamiento ante fallos:
        1. La instancia primaria falla.
           Cada segundo, la instancia primaria escribe en la base de datos del sistema una señal de latido (heartbeat signal). Si multiples señales no son detectadas y la replica failover esta sana, el mecanismo de failover inicia. Esto ocurre si la instancia primaria no responde por aproximadamente 60 segundos o si la zona primaria experimenta un corte de energía.
        2. Cloud SQL espera a que la replica failover alcance el último estado de la instancia primaria (replicación de datos). Este monto de tiempo que esto toma esta directamente influenciado por el [delay de la replicación de datos](https://cloud.google.com/sql/docs/mysql/high-availability#replication-lag).
        3. La replica failover es promovida como instancia primaria. Tener en cuenta que esta instancia recibe conexiones desde la zona secundaria pero usando la IP de la instancia primaria, haciendo que los clientes de base de datos no necesiten cambiar los parámetros de conexión.
        4. Una nueva replica failover es recreada. Si el fallo ha sido a nivel de instancia, la creación ocurre en la zona de la anterior instancia primaria; si ha sido a nivel de zona, la creación ocurre en una zona que se encuentre funcionando correctamente.
    <br/>

    - Una característica que diferencia Cloud SQL de su competencia es que es posible probar el comportamiento ante fallos, ya que desde la consola administrativa de Google Cloud Platform se puede [iniciar el failover](https://cloud.google.com/sql/docs/mysql/configure-ha#test). Esto permite que los clientes MySQL que se usan en las aplicaciones de TCI puedan estar preparadas de antemano para un escenario de fallos.

    - Para mayor información sobre detalles más avanzados del funcionamiento de alta disponibilidad en Cloud SQL, revisar el siguiente enlace: https://cloud.google.com/sql/docs/mysql/high-availability
<br/>

- **Replicacion**
    <br/>

    - Cloud SQL provee la habilidad de replicar una instancia primaria hacia una o más replicas de **solo lectura**. Una replica de solo lectura es una copia de la instancia primaria que refleja sus cambios de forma casi inmediata.
    ![Configuración HA](//pictures/gcp/ha-mysql-normal.svg)
    <br/>

    - Para mayor información sobre los detalles de replicación, revisar el siguiente enlace: https://cloud.google.com/sql/docs/mysql/replication/ y https://cloud.google.com/sql/docs/mysql/replication/tips
    <br/>

- **Backups**
    <br/>

    - Cloud SQL **retiene hasta 7 backups automáticos** por cada instancia primaria. En el caso de las instancias de segunda generación, el uso de storage para backups es cobrado a un precio reducido.
    <br/>

    - Con respecto al tamaño de los backups, estas son incrementales; esto quiere decir que solo contienen la data que ha sido modificada desde el último backup. Si el último backup es eliminado, el anterior a este pasa a obtener la data del anterior más el diferencial que este backup guardaba, haciendo que siempre se tenga una imagen completa de la base de datos.
    <br/>

    - Los backups son creados de manera automática, dentro de un rango de 4 horas especificado al momento de activar la característica de backup automático. Se recomienda que los backups programados se ejecuten cuando la instancia tenga la menor cantidad de carga.
    <br/>

    - **Solo para las instancias de segunda generación** los backups tambien pueden ser creados a demanda.
    <br/>

    - El borrado de backups depende de como estos hayan sido creados: Si ha sido de forma automática, cuando se llegue al límite de 7 backups se borra el más antiguo. En caso de un backup hecho a demanda, este debe de ser borrado de forma manual.
    <br/>

    - **Solo para las instancias de segunda generación** los backups son guardados en dos regiones, para obtener redundancia.
    <br/>

- **Restauración**
    <br/>

    - Durante la operación de restauración, la instancia primaria no se encuentra disponible para conexiones entrantes. Las conexiones existentes son cortadas. Para otros requerimientos y tips sobre la restauración de datos, visitar el siguiente enlace: https://cloud.google.com/sql/docs/mysql/backup-recovery/restore     
    <br/>

- **Seguridad**
    <br/>
    - La data que se guarda en Cloud SQL es encriptada en el instante en que ingresa a la red interna de Google y cuando es guardada en tablas, ficheros temporales y backups. Cloud SQL soporta conexiones privadas a traves de su servicio [Virtual Private Cloud (VPC)](https://cloud.google.com/vpc/) y cada instancia de Cloud SQL incluye un firewall para administrar quien puede interactuar con la base de datos. Este producto cumple el [ISO/IEC 27001](http://www.iso.org/iso/home/store/catalogue_ics/catalogue_detail_ics.htm?csnumber=54534), junto con otros estándares como: SSAE 16, PCI DSS v3.0 y HIPPA.
    <br/>

    - Adicional al filtrado de conexiones por firewall, Cloud SQL ofrece otro mecanismo de conexión para aquellas estaciones de trabajo que no tengan una IP estática o no puedan configurar una conexión SSL, [Cloud SQL Proxy](https://cloud.google.com/sql/docs/mysql/sql-proxy).

- **Metricas**
    <br/>
    - Similar a Azure, GCP brinda un conjunto de métricas para todos sus servicios y estas pueden ser observadas a traves de su servicio Google Stackdriver: https://cloud.google.com/stackdriver/. La lista de métricas disponibles se encuentra en: https://cloud.google.com/monitoring/api/metrics_gcp#gcp-cloudsql.
    <br/>

    - Con respecto a los logs de la base de datos, estas pueden ser observadas a traves de la interfaz web de Google Cloud Platform (https://console.cloud.google.com/). El periodo de retención de los logs son de 30 días.
    <br/>

#### Costo
- Basados en la documentación de Cloud SQL: https://cloud.google.com/sql/pricing, se estima el costo para la region de Iowa

    | Recurso              | Descripción                                | Precio Total (x mes)  |
    | -------------------- |:------------------------------------------:| --------------------: |
    | Instancia            | Segunda Generacion MySQL: db-n1-standard-4 | $197.25               |
    | Instancia (failover) | Segunda Generacion MySQL: db-n1-standard-4 | $197.25               |
    | Storage              | 500GB SSD ($0.17 x GB)                     | $85.00                |
    | Backup               | 500GB ($0.08 x GB)                         | $40.00                |
    | Transf. de Datos     | Ingress                                    | $0.00                 |
    | Transf. de Datos     | Egress (IBM -> GCP): $0.19 x 100GB         | $19.00                |
    | Reserva IP address   | --                                         | $7.30                 |
    | **Total**            | --                                         | **$545.00**           |
<br/>

- Si hicieramos una migración completa de nuestros servicios a GCP los siguientes precios cambiarian:

    | Recurso              | Descripción                                         | Precio Total (x mes)  |
    | -------------------- |:---------------------------------------------------:| ---------------------:|
    | Transf. de Datos     | Egress (GCP -> GCP ): $0.0 x 100 GB                 |  $0.00                |
    | **Total**            | --                                                  | **$526.00**           |
<br/>

---
## MongoDB Atlas
MongoDB Atlas es una base de datos como servicios creada por los expertos detras de MongoDB. Atlas provee todas las características de MongoDB pero removiendo toda la carga de infraestructura, brindando alta disponibilidad y replicacion de datos desde el inicio. Esto permite que las empresas se centren unicamente en lo que más les interesa a ellos y a sus clientes. 

Atlas se encuentra disponible para los siguientes proveedores de nube, en las siguientes regiones/zonas:
![Caracteristicas Por Capa I](//pictures/mongodb_atlas/atlas_proveedores_regiones.png)
<br/>

Los tipos de instancias que posee Atlas dependen del proveedor de nube. Por ejemplo, para Google Cloud Platform:
![Caracteristicas Por Capa I](//pictures/mongodb_atlas/atlas_gcp_instancias.png)
<br/>

La documentacion de Atlas indica que puede soportar alta disponibilidad en su servicio, pero esta solo es lograda mediante un replica-set (un cluster de servidores MongoDB que implementan replicacion y failover automática). Basados en la documentación de MongoDB: https://docs.mongodb.com/manual/replication/ y https://docs.mongodb.com/manual/core/replica-set-architectures/, **para formar un replica-set se requiere un minimo 3 nodos**. La estimacion brindada por Atlas está basado en este modelo.

Los costos de transferencia de datos se estiman en base al proveedor de nube seleccionado.

El costo de backup es estimado por replica-set varia entre $1.50-$2.50 por GB, por mes. La variación depende la periodicidad de los backups. Atlas indica que para mayor información sobre el manejo de backups, es necesario contactarse con su equipo de ventas.

Con respecto a la seguridad, el servicio ofrecido por Atlas corre sobre una VPC sea cual sea el proveedor de nube elegido, brindando por lo tanto un buen nivel de isolación de nuestra data con respecto a otros usuarios de Atlas. Encriptacion de los datos sobre la red y control de accesos son configurados por defecto. También nos permite indicar cuales IP son las únicas que pueden ingresar a la base de datos (IP whitelisting). Mayor información sobre los mecanismos de seguridad pueden ser consultados en las siguientes urls: https://www.mongodb.com/cloud/atlas/faq, https://webassets.mongodb.com/_com_assets/collateral/Atlas_Security_Controls.pdf?_ga=2.245079750.255796718.1539128870-1635592663.1532831157

#### Costo
- Basados en la documentación: https://www.mongodb.com/cloud/atlas/pricing, se estima el costo para la region us-west1 (Iowa)

    | Recurso              | Descripción                                | Precio Total (x mes)  |
    | -------------------- |:------------------------------------------:| --------------------: |
    | Instancia            | M30 (7.5GB RAM / 40GB storage))            |                       |
    |                      | 160GB de storage                           | $430.27               |
    | Backup               | 160GB ($1.5 x GB)                          | $240.00               |
    | Transf. de Datos     | Ingress                                    | $0.00                 |
    | Transf. de Datos     | Egress (Depende del proveedor de nube)     |                       |
    | **Total**            | --                                         | **$670.00**           |

